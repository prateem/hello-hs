module MyLib (someFunc, Address(..)) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

type Building = String
type AptNo    = Integer
type Street   = String
type City     = String
type State    = String
type Zip      = String
data Address  =
  Address { building :: Building
          , aptno    :: (Maybe AptNo)
          , street   :: Street
          , city     :: City
          , state    :: State
          , zip      :: Zip
          } deriving ( Show
                     , Read )
