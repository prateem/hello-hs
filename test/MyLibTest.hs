module Main (main) where

import Test.Hspec
import Test.QuickCheck
import MyLib

main :: IO ()
main = hspec $ do
  describe "Dummy test 0" $ do
    it "1 + 1 is greater than 1" $ do
      (1 + 1) > 1 `shouldBe` True
    it "1 + 1 is not less than 1" $ do
      (1 + 1) < 1 `shouldBe` False
  describe "Dummy test 1" $ do
    it "2 + 2 is greater than 2" $ do
      (2 + 2) > 2 `shouldBe` True
    it "2 + 2 is not less than 2" $ do
      (2 + 2) < 2 `shouldBe` False
  describe "Dummy test 2" $ do
    it "x + 1 is greater than x" $ do
      property $ \ x -> x + 1 > (x :: Int)
    it "concat of nested list of strings is of length atlease 0 " $ do
      property $ ((\ xs -> (length . concat . concat . concat $ xs) >= 0) :: [[[[String]]]] -> Bool)

-- >>> hspec $ do describe "" $ do it "" $ do True `shouldBe` True
-- Ghci1[16:13]
--   Ghci1[16:30]
-- Finished in 0.0121 seconds
-- 1 example, 0 failures

-- >>> quickCheck ((\ x -> x + 1 > -32) :: Integer -> Bool)

-- -33

