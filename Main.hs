module Main where

import qualified MyLib (someFunc, Address(..))

main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  MyLib.someFunc

type FName = String
type LName = String

greet :: FName -> LName -> IO ()
greet fn ln =
  do
    putStrLn msg
    where
      msg = "Hello" <> " " <> fn <> " " <> ln <> "!"

address = MyLib.Address "24" Nothing "Downs Grove" "Basildon" "Essex" "SS16 4QL"
-- >>> greet "Prateem" "Mandal"
-- Hello Prateem Mandal!

