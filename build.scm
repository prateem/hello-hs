(use-modules
 (guix packages)
 (guix build-system haskell)
 (guix licenses)
 (guix gexp)
 (gnu packages haskell-check))


(package
 (name "hello-hs")
 (version "0.1.0.0")
 (source (local-file "." #f #:recursive? #t))
 (build-system haskell-build-system)
 ;; (arguments '(#:configure-flags '("--enable-silent-rules")))
 (native-inputs `(("ghc-hspec" ,ghc-hspec)
		  ("ghc-quickcheck" ,ghc-quickcheck)))
 (synopsis "Basic haskell package")
 (description "This package has one executable that prints \"Hello, Haskell!\"")
 (home-page "")
 (license gpl3+))
